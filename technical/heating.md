# :volcano: Heating system

Since beginning of winter 2021, Kanthaus is powered by an air heatpump heating system which is partly installed in the garden next to K20 garden door (actual heater), in K20 "water storage room" (some connections, valves, filters, meters) and the rest of the house (pipes, radiators).

## User guidelines

What is special about a heatpump heating system is that to be efficient, the heating system's water temperature has to be as cold as possible, while still keeping the house warm. That means:

* You can not heat up a room quickly - turn on the radiator at least 4 hours before you want to notice some warmth, expect a room to becomme "warm" only 2 days after turning on the radiator
* The maximum reachable temperature in a room is given by the settings of the central heating systems, normally should be around 19.5 °C in Kanthaus
* To keep the efficiency of the system high, we actively decided that "badly heatable rooms" (e.g. with too small radiators for the specific room) stay colder. An incomplete list (generally notice badly heatable rooms by looking at the 6 surfaces of the room: The more cold rooms are on the other sides, the worse heating works)
    * The Private (~13 degrees)
    * Freeshop Lounge (~13 degrees)
    * Cloud room (~14 degrees)
    * Hipster room (~13 degrees)
    * Sleep kitchen
    * Dragon Room

Additionally, a heatpump works very differently from a gas heating system and likes to run continously, as well as needs a minimum water flow through the radiators to work at all. This means at least 6 radiators need to be turned on all the time.

The heating system is normally running all day, heating power is slightly reduced during nighttime. It is normal and wanted that the heatpump outside is working all the time.

When it is warm outside (> 5 degrees), we might turn off the heatpump during nighttime.

## Troubleshooting

* A radiator is not getting warm, although the heating system should be running?
    * Is it warm outside? Maybe the heating system turned off temporarily during nighttime?
    * Try another radiator in another room, preferably on the other side of the house or in the other house
        * If successful, a part of the heating system might be turned off at the valves in K20-B-old-heating-room or K22-b-event-storage. Find people who might be working on the heating system before turning it back on.

If both are not successful, go to the heating system panel in K20-1 hallway.

* It does not show anything?
    * Locate the heating systems fuse in the main electricity fuse box in the electricity room in the basement
    * When you open the right panel of the heatpump outside (losen 4 screws, push it down, yes, push it down harder!) there is also 2 fuses at the top.

* It shows an error?
    * H62: Too many radiators are turned off so the heatpump could not satisfy its minimum water flow. Turn on some more radiators and make sure that nobody can turn them off (e.g. by removing the thermostat to have it fully on)
    * H70: Check the fuse "heater" in the main electricity box on the bottom right
    * H74: Maybe the house bus node controlling the heating system had a problem? Press the "On" button and watch it for 5 minutes, if it reappears, disable "Optional PCB connectivity" in the installer menu to make it completely autonomous again.

* I really want to heat a badly heatable/cold room/ I need to heat a room quickly because I forgot early enough.
    * Use an electric heater ("Heizlüfter"). We don't have any right now, but this means we should acquire one.
    * Isn't this very expensive?
        * Yes (60 cents per hour), but adjusting the heating system so this is possible without the electric heater is more expensive all the time and this scenario should not happen daily.

* I would like to permanently use a badly heatable room. What do I need to do?
    * Install fans underneath the radiator for ~50 percent increased power output
    * Install a bigger radiator
    * If this need is really there for many rooms, we might want to increase the hole heating systems temperatures, this increases the heating cost about 2.5 percent per degree (+ the increased heating cost for the rooms getting warmer of 6% per degree of room temperature)

* The heatpump is annoyingly loud, can we do something?
    * Depends. Is it a certain time or mode of operation? We can always have it operate on lower power for some hours, where it will then be more silent. Communicate your wish :-)

## Advanced information

The heatpump is a **Panasonic Aquarea T-Cap 12 kW WH-MXC12H9E8**.
[Service Manual](/assets/aquarea-service-manual.pdf)
[Planungs- und Installationsanleitung](/assets/aquarea-installation-manual.pdf)

In the K20 basement water storage room, there is two filters that should be cleaned regularly (e.g. at least once per heating season in the future, currently I do it weekly to get old dirt out of our system):

* Spirotap MB3 (The "big" thing in the return line on the bottom)
    * Turn off the heatpump, so the pump is not running
    * Take off black magnetic cap to the bottom
    * Get a bucket, put it underneath the exhaust in the bottom and turn the knob using the cap fully on for a second
    * **Careful** water comes out with high pressure!
    * Put black magnetic cap back on, turn heatpump back on
* Superfilter (The ball valve with the black handle on the bottem)

### Housebus integration
Because of some shortcomings of the heatpump controller, it is additionally monitored via a [HouseBus node](https://github.com/NerdyProjects/HousebusNode_Heatpump).
The housebus node is a new generation coming from our other nodes as it uses upgraded uavcan v1 and an arduino base stack.

It allows reading all status from the heatpump, the heatmeter and the electric counter.

The data is recorded in InfluxDB and can be watched at https://grafana.yunity.org/.

## Heishamon interface
[Heishamon](https://github.com/Egyras/HeishaMon) is a small piece of open-source software and hardware to monitor and control the heatpump. It is located in the K20-B water room and connected to WiFi.

It also emulates the `Optional PCB` thus is able to set the SG ready signals and demand control.

Heishamon is used to adjust the water target temperatures, with the aim to keep room temperatures consistent and use more of our own solar power.
