# :ghost: Network setup

This page should document some bits of the network infrastructure of kanthaus. Please maintain so people with a bit of network knowledge can understand and maintain the network.

## Overview
![Network topology](/images/network-topology.svg)
<!--
drawn with https://excalidraw.com/
raw file for modifying: /images/network-topology.svg
-->

## TRUNK network
the backbone of our network, connecting all Access Points and the central firewall, carries all the different networks via VLAN:

| VLAN-ID| IP Range      | Name       | Purpose / Devices
|-------:|---------------|------------|----------
|untagged| 192.168.178.x | ADMIN      | configuration tnterfaces of all AP's & Switches
|      4 | 192.168.4.x   | PRINTER    | connection between print server (kanthaus-server) and printer
|      5 | 192.168.5.x   | IOT _(former SMA)_| only access to kanthaus-server and potentially whitelisted internet IP's + isolated clients¹
|    100 | 192.168.100.x | RESTRICTED | normal internet access + isolated clients¹
|    101 | 192.168.101.x | VPN        | tunneled to VPN (currently NL)
|    102 | 192.168.102.x |UNSECURE   | normal internet access

¹ client isolation does not work between LAN <-> LAN clients

## Wifi's
| SSID               | Network    |  Encrypt. | 802.11r | 802.11w | Isolation | Comment
|--------------------|------------|:--------:|:-------:|:---------:|:---------:|------------
| `kanthaus`         | RESTRICTED | WPA3     | -¹      | required  | on        |
| `kanthaus-insecure`| INSECURE   | WPA2     | -       | -         | -         | unsupported drivers, LAN parties, etc.
| `kanthaus-gast`    | VPN        | -        | -       | -         | on        |
| `kh-admin`         | ADMIN      | WPA3     | -       | required  | -         |
| `kh-iot`           | IOT        | WPA2     | -       | -         | on        |

**802.11r:** Fast Roaming
**802.11w**: Management Frame Protection

¹ would be cool to have it on, but eventually led to issues. let's reevaluate in a few months

## Device list
| Type        | Name          | Admin-IP       | Location      | Hardware        | Comments
|-------------|---------------|----------------|---------------|-----------------|-------
| Router      | `fritz.box`     | 192.168.200.1  | K20-1 hallway    | FritzBox 7530   | DSL termination (on main network), telephone
| Router      | `firewall`      | 192.168.178.1  | K20-B rack     |                 | central router / firewall
| AP          | `dragon-k20-0`  | 192.168.178.207 | K20-0 hallway | Archer C5 v1.2  |
| AP          | `dragon-k20-1`  | 192.168.178.201 | K20-1 hallway | Archer C5 v1.2  |
| AP          | `dragon-k20-3`  | 192.168.178.208 | K20-3 center | Archer C5 v1.2  |
| AP          | `dragon-k20-B`  | 192.168.178.206 | K20-B water room | Archer C5 v1.2  |
| AP          | `dragon-k22-0`  | 192.168.178.204 | K22-0 side hallway| Archer C5 v1.2|
| AP          | `dragon-k22-1`  | 192.168.178.202 | K22-1-4 piano room| Archer C5 v1.2  |
| AP          | `dragon-k22-2`  | 192.168.178.203 | K22-2 hallway     | Archer C5 v1.2|
| AP          | `dragon-k20-outside`|192.168.178.205| K20-0-2 window| Archer C5 v1.2 | provides uplink for flat
| ~~DECT base~~   | ~~`dect-k20-1`~~    | ~~192.168.178.3~~  | ~~K20-1 hallway~~  | ~~fritz!box 7330~~ | ~~currently only used by one ohone for testing~~
|Managed Switch| k20 firewall switch| 192.168.178.9 | K20-B rack    | TL-SG108E
|Managed Switch| k22 trunk switch  | 192.168.178.10 | K22-B stairs      | GS108E v3
| Switch      | k20 trunk switch | - | K20-B Rack        |            | provides PoE for some AP's in K20
| Server      | `kanthaus-server` | 192.168.\*.2 | K20-0 hallway      | Server, i5-2500K, 16GB Ram | file storage, nextcloud, foodsharing gitlab CI Server, housebus logging & time/sunset provider (see [Repo](https://gitlab.com/kanthaus/kanthaus-server-services/))
| Main water meter reader | `mainwatermeter` | 192.168.5.37 | K20 basement (former heating room)      | ESP32-CAM | see [Water usage tracking](./water.html)
| Warm water meter reader | `warmwatermeter` | 192.168.5.38 | K20 basement (rainwater room)     | ESP32-CAM | see [Water usage tracking](./water.html)
| Ventilation watcher | `ventilation` | 192.168.5.39 | K20 attic | ESP32-WROOM | see [docs](https://gitlab.com/kanthaus/ventilation-watcher)
| K20 door | `k20-door` | 192.168.5.40 | K20 hallway | ESP32-CAM| see [docs](https://gitlab.com/kanthaus/door-esp32)

## Firewall / Router
central point of all subnets, routes between them
- runs OPNSense
- due to lack of ports on firewall, a VLAN switch added, bound to the firewall, also connecting to the fritz!box
- Features:
    * VPN client
    * DNS Server
    * DHCP Server

## Access Points
- see Device list
- so far all Archer C5 v1.2
- centrally configured by OpenWISP: https://openwisp.im.kanthaus.online (only reachable inside the `ADMIN` net)
- starting point for other ethernet cables on that floor
- in K20 all powered by PoE (802.3af) from the switch in the basement


## Printer (Canon C2025i)
- Printer is in its own subnet together with the kanthaus-server
- On the kanthaus-server, there is _CUPS_ running with printer sharing and auto discovery in the networks via avahi-daemon
- In `RESTRICTED` and `UNSECURE` the printer is also reachable directly via 192.168.4.153:9100

## kanthaus-server
- Most of the services are running inside a docker-compose setup
    - Repo: https://gitlab.com/kanthaus/kanthaus-server-services
    - in `/opt/kh-services`
    - checking state: `docker-compose ps`
    - starting everything: `docker-compose up -d`
    - logs: `docker-compose logs -f --tail=20`
- BTRFS raid for HDD's under `/data`


## Random notes

### Building customized OpenWRT for Archer C5
```sh
ssh kanthaus-server
sudo -iu openwrt-builder
cd openwrt
make
ls -l bin/targets/ath79/generic/
```

##### included changes
* root password
* default IP `192.168.178.200` in ADMIN range
* switch: all ports in ADMIN net
* disabled DNS rebind protection
* wireless interface names
* additional packages
    * openwisp-config
    * prometheus-node-exporter-lu
    * luci-ssl (for https)
    * ebtables (for [effective client isolation](https://blog.matthias-larisch.de/openwrt_client_isolation/))
    * tcpdump (for easier debugging)


### How to update OPNsense

There is a short downtime involved during the two reboots, usually 2 minutes each.

1. Join the `kanthaus-admin` wifi network and log in to 192.168.178.1 (user `root`, password in keepass). Export the configuration to make a backup.
2. Check if updates are available (System->Firmware->Updates)
3. In case of minor updates, just use the button to perform 
4. If there is a major update (2x per year), download the newest version from https://opnsense.org/download/, unzip and flash to any USB stick
5. Take a VGA monitor, a USB keyboard and the USB stick to the K20 basement big room, open the rack box and connect the devices to the small computer (HP)
6. Log in with the USB keyboard (same credentials as above) and trigger a reboot. It should now boot from the USB stick.
7. Press any key when it prompts to run the "Importer". Now it will boot up a live environment and read the configuration file. If successful, network services should run as before.
8. If all looks good, it's time to install to disk. Run `opnsense-installer` from the shell (either on local keyboard or via ssh), choose ZFS and let the installer do its job.
9. Remove the USB stick and reboot again.
10. Check if the system came up good.
11. Perform any last updates from the web UI.
12. Delete the configuration backup again, it can contain secrets.


### Pitfalls
* We use 802.1q VLAN tagging. All switches everywhere need to have at least passive passthrough support, otherwise the Vlans disappear at that switch. I don't know of any gigabit switch that does not support this.
* Again Vlan: Managed switches normally need to have all VLANs that should be forwarded (also tagged -> tagged forward) defined in them.
* Again Vlan: Some Access Points like WDR841 v7/v8 cannot handle tagged and untagged vlan on the same port at the same time. So far, we don't have any equipment like that and likely we will never have, but just to know...
* Again Vlan: fritz boxes have their switch in managed vlan mode and don't forward any tagged vlan
