# :fax: Printing and Scanning

## Canon imageRunner Advance C2025i
* Location: Office (K20-1-4)
* Type: Laser
* Color: CMYK
* Paper size: A4 (Tray 2), A3 (Tray 1), Custom (Manual Feed)
* Duplex: Yes
* Scanning: Duplex ADF, to network share or USB

## Printing

### Good to know
- You need to be in the `kanthaus` network to print
- The printer consumes ~15W on standby. It may be left on during the day; it should be switched off at night. The power switch is on the right, vertical edge as you face the printer, under the black cover.
- On startup the printer may ask for the paper size of a paper tray: choose _"A4 - plain paper"_.
- Sometimes the printer doesn't wake up from sleep for printing: wake it by pressing the 'power' button on top panel.
- Pressing the 'eye' button 👁 let's you view the current print job(s) and adjust the paper size.
- Often pages are printed in _"US Letter"_ format accidentally - but there is only A4 and A3 paper in the printer, so these jobs are rejected with the _"Load paper"_ instruction.

### Manual installation

#### Linux
1. Install the Canon CQue driver:
    * Arch/Manjaro: `yay -S canon-cque`
    * Debian/Ubuntu: download and install the [CQue DEB Driver](https://www.canon-europe.com/support/business/products/office-printers/imagerunner/advance/imagerunner-advance-c2025i.html).
2. Add printer:
    * via system settings:
        1. _"Settings"_ → _"Printers"_ → _"Additional Printer Settings..."_ → _"+ Add"_ → _"Network Printer"_ → _"AppSocket/HP JetDirect"_
        2. For _"Host"_ give `192.168.4.153` → _"Forward"_
        3. Pick the driver
            * In _"Makes"_ select _"Canon"_ → _"Forward"_
            * In _"Models"_ choose _"iR-ADV C2025i"_ and in _"Drivers"_ choose _"Canon iR-ADV C2025i PCL"_ → _"Forward"_
                * if not available, choose _"iR-ADV C2025 UFR II"_ or _"iR-ADV C2025i PXL"_
        4. For all further options you can keep the default 
    * via CUPS:
        1. Open CUPS interface: http://localhost:631
        2. _"Administration"_ → _"Printers"_ → _"Add Printer"_ (You may be asked for your user (sudo) credentials)
        3. _"Add Printer"_ → _"AppSocket/HP JetDirect"_ → _"Continue"_
        4. For _"Connection"_ give `socket://192.168.4.153:9100` → _"Continue"_
        5. Provide _"Name"_ of your choice. _"Description"_ and _"Location"_ can be left empty → _"Continue"_
        6. For _"Make"_ select _"Canon"_ → _Continue_
        7. For _"Model"_ select _"Canon iR-ADV C2025i PCL"_ → _"Add Printer"_
3. Ensure _"Media Size"_ is _"A4"_ in the printer settings.

#### Windows
1. Download and extract the [[Windows 32bit & 64bit] Generic Plus PCL6 Printer Driver](https://www.canon-europe.com/support/business/products/office-printers/imagerunner/advance/imagerunner-advance-c2025i.html)
    * Make sure you know where the driver is extracted to. It may be in the same folder as the `.zip`, or it might have been cleverly put somewhere by your system. If unsure, try to extract the `.zip` again: it will complain that it's already been extracted, and show you the path where it's being extracted to.
2. Add printer
    * Windows 10
        1. _"Start"_ → _"Settings: Printers and Scanners"_ → _"Add a printer or scanner"_ Ignore auto-detected printers, click _"The printer that I want isn't listed"_ that will appear in some moments.
        2. In _"Find a printer by other options"_ select _"Add a local printer or network printer with manual settings"_ → _"Next"_
        3. In _"Choose a printer port"_ select _"Create a new port"_, and in _"Type of port"_ select _"Standard TCP/IP Port"_ → _"Next"_
        4. In _"Type a printer hostname or IP address"_, for _"Hostname or IP address"_ give `192.168.4.153`. _"Port name"_ will auto-fill as you write, leave it as is. Make sure _"Query the printer and automatically select the driver to use"_ is selected → _"Next"_
        5. In _"Additional port information required"_ leave the _"Device Type"_ as _"Standard: Generic Network Card"_, → _"Next"_
        6. In _"Install the printer driver"_, select _"Canon Generic Plus PCL6"_ either through the options list, or by navigating via _"Have Disk..."_ → _"Next"_
            * If you use _"Have Disk..."_ the path will start with the folder you extracted to (see `1.`) and end with `GPlus_PCL6_Driver_V290_32_64_00\x64\Driver\CNP60MA64.INF` or similar.
        7. If you get to _"Which version of the driver to you want to use?"_ leave the selection as _"Use the driver that is currently installed (recommended)"_
        8. In _"Type a printer name"_, give the printer a meaningful name for you → _"Next"_
        9. In _"Printer Sharing"_, leave _"Do not share this printer"_ selected → _"Next"_
        10. In _"You've successfully added [name]"_ → _"Finish"_
3. Ensure _"Page Size"_ is set to _"A4"_
    * Windows 10
        1. _"Start"_ → _"Settings: Printers and Scanners"_ → click on the printer you just added → _"Manage"_
        2. In the new window, click _"Printing Preferences"_ from the left-side menu
        3. In the new window, set _"Page Size"_ to _"A4"_ → _"Apply"_ → _"OK"_
        4. Now try printing the test page.

### Automatic
**Note:** _currently buggy. If it doesn't work for you, consider the manual installation in the next section._

If you are logged into `kanthaus` or `kanthaus-insecure`, the printer should automatically appear in your print dialogues as `Kanthaus_CanonC2025_...`. (The printer is _not_ accessible from `kanthaus-gast`.)

If you don't see the printer, check your system has [MDNS](https://en.wikipedia.org/wiki/Multicast_DNS) For linux, this means having `avahi-daemon` installed and running, which should be the default on most distibutions.

This method prints via the virtual printer installed on the kanthaus server. While this should normally work, you might consider installing the printer manually on your system to avoid any potential problems and have a more direct relationship to the printer.

## Scanning

## Scanning to USB
Insert a USB stick into the printer (slot to the right of the control panel). Press the power button on top to wake printer if sleeping, then press the _"Scan & Store"_ button on the top of the screen and select the USB Memory Media.

Select options as desired, then press big, green button to scan. Press _"Start storing"_ to transfer the scan, and finally eject USB stick via small, green button on the bottom-right corner.

> **Note:** The USB stick needs to be formatted with the Windows file system FAT to be recognised by the printer/scanner-combination.

## Scanning to Network
After turning the printer on, press the _"Scan & Store"_ button on the top of the screen and select the KH-Server location.

After scanning, you will find your files in the [Scan](smb://kanthaus-server/scan) share on kanthaus-server. Use the file manager of your system and browse for `windows/samba/network shares` or directly enter the address: `smb://kanthaus-server/scan`. If you are asked for any credentials, you can login anonymously.

For Windows 10, you may additionally need to:
1. enable SMB1.0
    - in settings, go to _"Windows Features"_ → tick options starting _"SMB"_ → click _"OK"_
2. map network drive
    - open file explorer → go to _"This PC"_ → click _"Map network drive"_ (top menu bar)
    - in the pop-up:
        - for _"Drive"_, choose a drive letter (any unused letter, default should be OK)
        - for _"Folder"_, give `\\kanthaus-server\scan` → _"Finish"_

> **Note:** Remove your files after scanning: they are accessible to anyone without authentication from all networks except `kanthaus-gast`.

