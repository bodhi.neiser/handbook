# :signal_strength: Wi-Fi

To cover as many different needs as possible, we have different Wi-Fi networks:

## `kanthaus-gast`:  simply and quick
- **no password**, so open also for the neighborhood
- The internet is rather slow, **not recommended for video conferences**
- Your traffic will be routed through a Dutch VPN provider. Please make sure that you encrypt your traffic yourself appropriately or do not consider this network to be secure.

## `kanthaus`: most secure, faster, printing, ...  and recommended!
- Ask for the password
- requires up to date devices. If it doesn't work for you use `kanthaus-unsecure`

## `kanthaus-insecure`: less secure, but more flexible
- Ask for the password
- Only here devices are allowed to see each other