# :fallen_leaf: Compost Toilet

You can find the compost toilet in the K22 staircase, the room with the actual toilet is inbetween the ground floor and the first floor, the "tank room" where the material is composted is right below it on the ground floor, next to the garden door.

## General principle
* Urine is separated and directed into the normal sewage system, the urine tube forms a siphon to prevent smell from the sewers
* The poop falls into a 800l plastic tank (which used to be an oil tank)
* The tank has a grid of metal bars on the bottom that allows for airflow and "harvesting" of the composted materials from below
* The tank also has a sewage connection on the very bottom in case there is ever an overflow or the material becomes too wet 
* A 120mm 3W PC fan creates a slight vacuum throughout the tank and toilet and blows unwanted smells outside just below the roof

Details about the priciple and instructions on how to build your own indoor compost toilet can be found in the brochure "Einfälle statt Abfälle" by Christian Kuhtz, which you can find in some KH bookshelf :smile:

## Usage hints

* Because of the urine seperation the material can get too dry. It might make sense to pee into the tank from time to time. Best have 1 or 2 people who use it regularly always pee beside the urine seperation funnel (currently, 2022-07, Janina does this)
* Emptying the tank is nessecary 1-4 times a year, depending on how much it is used
* If the material stacks up all the way to the ventilation outlet, its probably time to empty soon. You can buy some time by using the metal rod from the tank room, to push the hill of accumulated material down to the sides


## Emptying procedure

### Prequisits:
* approx. 1-2 hours of time
* tools (should be stored in the front area of the tank room):
    * long metal rod, bend at the end
    * some penny-bags (1-4)
    * a shoveling device (one with short or no handle seems to work well)
* something to lay on, e.g. a camping mat (something that's easy to clean or where it doesn't matter if it gets dirty)
* a headlamp (can usually be found in the workshop, right when you enter to your left)
* maybe gloves (can also be found in the workshop)
* no arachnophobia :D yes, there will be spiders and other crawling insects

#### Emptying
1. <p>Start with finding a comfortable position lying on your back.</p>
<img src="../images/comfortable_position.jpg" alt="comfortable position" style="width: auto;" />
2. <p>Use the metal rod with bent tip to poke around between the bars of the grid, eventually you will have enough space to shift the material around in the tank and let it trickle down through the barsof the grid. </p>
3. If it does not work or it is too difficult, you can try to push a few of the bars to the sides (they are just loosly connected the the main carrying bars with some wire) to have more space to poke around with your rod.
4. If the material in the tank is to wet/heavy/dense it can help to poke into the tank from above (i.e. into the toilet) with the tip-bent rod.
5. <p>After enough material has accumulated in the area below the rod-grid, use the shovel to move it to a penny bag other other container<p/>
<img src="../images/shit_shovel.jpg" alt="shovel it up!" style="width: auto;" />
6. <p>Continue with step 1.</p>
7. Depending on how full the tank is and when it was last emptied, you can remove 2-6 penny bags. If the material changes from "feels like compost" to "feels like poop" you should stop :)
8. You can just empty the bags into the normal compost at the far corner of the garden, maybe try to mix it into the normal compost a bit with a shovel.
9. Don't forget to clean up and wash your hands!

 
## General maintanance / bug fixing

* clogged fan: happens either when a piece of toilet paper or similar gets stuck in front of the fan from the inside, or the fan itself or the pipe behind it are filled with sawdust/flied
    * remedy: go into the garden to what used to be the outside window of the "tank room" (you might have to move some trash bins), remove the piped leading out from the fan (the are all only loosly stuck together) and shake out the dust / use compressed air.
* broken fan / power adaptor: I would expect the fan to only last a few years under this stress, if is breaks you should find a new one in the electronics workshop / salvage old PCs or power supplied. Power adaptors you can also find in the electronics workshop, anything from 5-16V should work (but give you a more or less powerfull fan)
* if you for some reason need to access the tank from above, or something with be toilet itself is broken (urine tube or "poop slide" made of cut-open buckets) the wooden chasis is not screwed or glued, only held in place by some wooden dowels. You can just lift the top off the sides, the urine tube and cut-open buckets will be in the way though as they are quite tighly connected, so you will need to lift the top wooden piece together with the toilet seat, urine-tube and buckets.

## Possible future improvements

* add a butt-shower (there still should be some in the workshop, ask Silvan)
* integrate the sawdust bin into the main wooden construction of the compost toilet, mainly to have more space in comfort when sitting on the toilet, and for optical reasons. Its a bit tricky but should be possbile, maybe with a bin a bit smaller then the one used now. The bin should stay removable and you can use the cut-out wood as a lid. Ping Bodhi on slack if you are interested!

