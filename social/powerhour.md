# :sparkles: Power Hour

The weekly communal cleaning session.

### Time and place

**Thursdays at 10 am** we meet in the [dining room](/rooms/diningroom.md) to distribute tasks. Then all of our houses will be cleaned for a maximum of 2 hours. At noon somebody will go around and call people who are still working to Fika.

### Initial meeting

There is a Power Hour Task Board where all relevant tasks are listed.

![](PowerHourTaskBoard.jpg)

The facilitator goes through the tasks briefly to find out which ones are of priority this week. Visitors are given tasks first so that they can be paired with more experienced Kanthausians easily. Then there is a round in which people can grab their preferred task. The facilitator makes sure that no important task is left and that everybody has something to do in the end.

This meeting should not take more than 10 minutes.

### Frequent tasks

Now follows the detailed description of the frequent tasks.
The order might be understood as a rough prioritization.

1. Main Kitchen
  - Wash all dirty dishes that don't belong in the dishwasher
  - Sort away clean dishes
  - Empty the kitchen table
  - Clean the stove
  - Clean kitchen appliances if very dirty
  - Clean the surfaces


2. Snack kitchen + breakfast table
  - Clear the breakfast table
  - Empty the dishwasher if full of clean dishes
  - Fill the dishwasher with dirty dishes
  - Clean tea cupboard and move empty thermoses up
  - Throw away dubious leftover tea
  - Clean table and remove all crumbs
  - Clean microwave, small ovens and electric kettle if very dirty
  - Make bread table orderly and remove all crumbs
  - Clean sink and change hand towel


3. Bathrooms K20
  - Go to snack kitchen, grab some 'Badreiniger' and two sponges or cloths
  - Go to main bathroom, clean bathtub, mirror and sink with one utensil and toilet with the other
  - Change hand towel
  - Grab two more clean towels and the dirty laundry basket
  - Go to upper staircase toilet, clean mirror and sink with one utensil and toilet with the other
  - Exchange hand towel and take the dirty one with you
  - Go to lower staircase toilet, clean mirror and sink with one utensil and toilet with the other
  - Exchange hand towel and take the dirty one with you
  - Go to washing room and put dirty laundry there, wet things go directly into the machine


4. Bathrooms K22
  - Go to snack kitchen, grab some 'Badreiniger' and two sponges or cloths
  - Go to main bathroom and grab three clean towels
  - Go to K22-2 bathroom, clean bathtub, mirror and sink with one utensil and toilet with the other
  - Exchange hand towel and take the dirty one with you
  - Go to baby bathroom, clean bathtub, mirror and sink with one utensil and toilet with the other
  - Exchange hand towel and take the dirty one with you
  - Go to compost toilet, clean sink with one utensil and toilet with the other
  - Refill sawdust if necessary and swipe dust inside the toilet
  - Exchange hand towel and take the dirty one with you
  - Go to washing room and put dirty laundry there, wet things go directly into the machine


5. Vacuum team K20+K22/1
  - One of you operates the vacuum cleaner and one of you prepares the spaces
  - Take the vacuum cleaner from the snack kitchen (?)
  - Start in the dry food storage and work your way to the intermediate storage
  - The prep person–
    - moves things out of the way
    - shakes and then folds blankets on couches
    - makes couches look nice
    - beats chairs and puts them on tables


6. Vacuum team K20/2+0
  - One of you operates the vacuum cleaner and one of you prepares the spaces
  - Take the vacuum cleaner from ?
  - Start in the dorm and work your way to the cloud room
  - Make sure to vacuum the staircase
  - The prep person–
    - moves things out of the way
    - shakes and then folds blankets on couches
    - makes couches look nice
    - beats chairs and puts them on tables


7. Vacuum team K22/2+0
  - One of you operates the vacuum cleaner and one of you prepares the spaces
  - Take the vacuum cleaner from ?
  - Start in the hipster room and work your way to the free shop
  - Make sure to vacuum the staircase
  - The prep person–
    - moves things out of the way
    - shakes and then folds blankets on couches
    - makes couches look nice
    - beats chairs and puts them on tables


8. Vortex shifting
  - Move stuff in the intermediate storage room one phase up
  - Take pictures of the stuff that moved from phase 1 to phase 2 and post them to #kanthaus-stuff
  - Find new locations for stuff from phase 3: free shop, trash, whatever makes sense


9. Washing room
  - Clean all the food that is unwashed (can be found on the floor)
  - Carry up clean and dry food (can be found on elevated surfaces)
  - Clean sink and exchange hand towel (can be found in main bathroom)
  - If there is no food to be washed or you have time left go on otherwise just stop here
  - Clean boxes and bags that are standing around
  - Clean bathtub
  - Clean surfaces and make the room look as tidy as possible


10. Food sorting
  - Go to the shelf in K20 staircase
  - Sort though all the perishable food and throw away bad items
  - Make a box with food that needs to be eaten/prepared today
  - Clean surfaces if necessary
  - Remove empty boxes and bring them to the washing room
  - Go to the basement and repeat the process
  - Also make sure to remove paper waste
  - If the fridge in the snack kitchen is in use do the same there again


11. Communal closet sorting
  - First put up all baskets so that the vacuum person can vacuum
  - Then start folding everything neatly and sort it into the closets
  - Requirements here are too arbitrary to properly explain, sorry...
  - When done, bring down the empty laundry baskets and place close to the washing machine


12. Bin emptying
  - Go through all of the house and empty the bins
  - The big bins are outside, next to the K22 back door


13. Plant care
  - Go through the house and touch the soil of potted plants
  - If dry, add water - but don't let it overflow


14. General sorting
  - Go through all of the house and sort away stuff that doesn't belong
  - This is a very specialized task that requires a lot of knowledge


15. Mop
  - Start not before 11 please
  - Find utensils in the snack kitchen
  - Mop all of K20-1, K22-1, the main bathroom and wherever it is needed
