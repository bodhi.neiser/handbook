# :green_book: Handbook

This is documentation about the handbook itself :bowtie:

## :books: HonKit

We use [HonKit](https://github.com/honkit/honkit) to build the handbook.

## :clap: How to contribute

The content is stored at [gitlab.com/kanthaus/handbook](https://gitlab.com/kanthaus/handbook).

First, make sure you have access rights:
* create an account at [gitlab.com](https://gitlab.com) if you don't already have one
* ask someone in **#kanthaus** [slack](https://slackin.yunity.org) channel to give you the right permissions

Next, understand the two approaches you can take:
1. edit directly in the GitLab interface
2. clone the git repository to your computer, make changes, then push them back

### :cloud: Editing in GitLab interface

You can click the _EDIT THIS PAGE_ button at the top of any page, and it will take you to the appropriate page to edit on GitLab.

The content is written in markdown, if you are not familiar with markdown you can learn more about it at [markdownguide.org](https://www.markdownguide.org/).

Once you have made your changes you can optionally edit the comit message, then press _Commit changes_.

After a few minutes the changes will be live on the website!

### :computer: Editing on your local computer

For this you will (probably) need to become familiar with:
* the command line
* git
* editing markdown files
* honkit
* nodejs and yarn
* ssh keys (optionally)

Firstly, clone the repository:

```
git clone https://gitlab.com/kanthaus/handbook.git
```

(note if you want to use ssh keys you'll need to use `git@gitlab.com:kanthaus/handbook.git` instead).

You'll then have a directory called `handbook` wherever you ran the command. We need to enter the directory and install the dependencies:

```
cd handbook
yarn
```

Now you're ready to run it locally on your computer:

```
yarn serve
```

This will build the book and serve it up at [localhost:4000](http://localhost:4000), you can visit that in your favourite web browser.

Now you can edit the content, I recommend using [visual studio code](https://code.visualstudio.com/) for editing.

Any changes you make will trigger your browser to reload the new content!

Once you're happy with your changes, we need to commit them to the repository, and push them back to GitLab:

```
# tip: see which files you've added/changed
git status

# add all changes in your local directory
git add .
# ... OR add changed files individually
git add handbook.md

# tip: check status again to see which files you're about to commit
git status

# tip: see the content you're about to commit
git diff --staged

# note: make sure your commit message is informative!
git commit -m "Clarified the handbook page"

# push the changes back to GitLab
git push
```

After a few minutes the changes will be live on the website!

When you next come to edit, you should make sure you have the latest changes from
other people too:

```
# fetch changes from the remote GitLab server
git pull
```

If it tells you there are conflicts it probably means someone else has edited that
page and git does not know how to combine your changes (it can combine unrelated
changes quite easily).

You can try and teach yourself how it works (search for "resolve git conflict" or
similar) but also you can just ask someone to help you :) Git can be quite
confusing.

## :car: Deployment

We use GitLab CI to build the gitbook, then rsync to deploy it to yuca.yunity.org where it is served at [handbook.kanthaus.online](https://handbook.kanthaus.online/).

You can check the status or progress by viewing the [pipelines](https://gitlab.com/kanthaus/handbook/pipelines).

## :zap: Emojis

You can use emojis from this [emoji cheat sheet](https://www.webpagefx.com/tools/emoji-cheat-sheet/) :horse: :apple: :crossed_flags:
