# :e-mail: Email accounts

We have email accounts hosted on [Uberspace.de](https://uberspace.de/en/).

## Creating a personal account

Login to Uberspace's dashboard with Kanthaus' account and go to [the Email tab](https://dashboard.uberspace.de/dashboard/mail).
At the bottom of the page you will find "Virtual Mailboxes" section where you can add your own. Pick a fresh password for it.

You can access your account via the webmail at https://webmail.uberspace.de/

You can also configure the email account in your email client, with the following settings (which can generally be auto-detected):

* Server (both for IMAP and SMTP): gehrels.uberspace.de
* Connection security: STARTTLS
* IMAP port: 143
* SMTP port: 587

## Getting access to Kanthaus email

Once you have created a personal account, you can set it up so that it contains the general email received by Kanthaus (at the `hello` address)
as a subfolder. You can set it up as follows:

* SSH into `gehrels.uberspace.de` (username `kanthaus`). This can be done by password or SSH key.
* Navigate to your email folder (`cd users/firstname` where `firstname` is you email user name)
* Create a symlink to the `hello` account with `ln -s ../hello/ .hello`
* Add the `hello` folder to your subscriptions: `echo hello >> subscriptions`

There are some other folders inside our mailbox that you could be interested in mirroring in your own account:
* the "Mailinglists" folder, which receives messages from mailing lists of befriended projects and other initiatives in the area.
* the "Spam" folder, in which spam gets sorted automatically (and therefore not always accurately).

To subscribe to those as well, you can do again `ln -s ../hello/.Spam/ .hello_spam` and `echo hello_spam >> subscriptions` for the Spam folder for instance.

Once that is done you should have access to Kanthaus email in your email client.

When replying to Kanthaus email we recommend you leave the `hello` address in Cc: and Reply-To:, so that further replies are visible for others.

## Email filtering

The Spam filtering we use is [the default one offered by Uberspace](https://manual.uberspace.de/mail-spam/).

On top of that, we have extra rules to sort messages into folders, for instance for mailing lists. Those filtering rules are managed by [the Sieve system offered by Uberspace](https://manual.uberspace.de/mail-filters/).
If you want to change those filters, here are some possible steps:

* First, get hold of the password for our hello@ email account. It is in our password store.
* Then, install [a ManageSieve client](http://sieve.info/clients) and use it to connect to the hello@ address. You can pick from a wide range of clients. For instance, the `sieve-connect` command line interface is available on Ubuntu/Debian and works. The credentials required are:
  * Server: gehrels.uberspace.de
  * Port: 4190 (standard)
  * Username: The email address
  * The password for it
* Now you have access to the Sieve scripts available on the address and let you edit them.

