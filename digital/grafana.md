# :bar_chart: Grafana server

We use a [Grafana](https://grafana.com/) instance to monitor various things in the house, which can be accessed at https://grafana.yunity.org/.

## Getting access

You need a [GitHub](https://github.com) account and need to be part of the [karrot-dev organization](https://github.com/karrot-dev).

## Logging new quantities in Grafana

To track new metrics in Grafana you will need to store them in the underlying InfluxDB database, hosted at https://influxdb.kanthaus.online/. If you are writing from an IOT device running inside Kanthaus, running from the `192.168.5.*` subnet, then you cannot directly reach the internet: instead, you can point your device to `http://kanthaus-server:88/influxdb` which will proxy your requests to the outside.

You will need to supply credentials to be able to access InfluxDB: you can find those in our central password store. The InfluxDB database to use is `kanthaus`.

You can use [InfluxDB's API](https://docs.influxdata.com/influxdb/v2.2/write-data/developer-tools/api/) to regularly write new values to the database. For instance, with `curl` this could look like this:

```
curl --request POST \
"http://kanthaus-server:88/influxdb/api/v2/write?bucket=kanthaus/" \
  --header "Authorization: Basic YOUR_API_CREDENTIALS" \
  --header "Content-Type: text/plain; charset=utf-8" \
  --data-binary '
    airSensors,sensor_id=TLM0201 temperature=73.97038159354763,humidity=35.23103248356096,co=0.48445310567793615 1630424257000000000
    airSensors,sensor_id=TLM0202 temperature=75.30007505999716,humidity=35.651929918691714,co=0.5141876544505826 1630424257000000000
    '
```

See [InfluxDB's documentation about the line protocol](https://docs.influxdata.com/influxdb/v1.8/write_protocols/line_protocol_tutorial/) for details about the format of the data to post.

Once your quantity is logged in InfluxDB, you can create a new panel for it in Grafana. This can be done from Grafana's web interface directly.
