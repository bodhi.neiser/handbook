# :memo: Evaluation record

In the evaluation record, which is a file in [the private kanthaus repository on gitlab](gitlab.html), we record a summary of the [evaluations required by our positions system](../social/positionsandevaluations.html). At the end of an evaluation, the facilitator is
expected to record the evaluation in this table.

### Structure

The evaluation record is a table (written in CSV format), containing for each evaluation the following fields:
- Date
- Person being evaluated
- Position before the evaluation
- Position applied for
- New position
- People who got to vote on the position
- Other non-voting people present at the evaluation
- Notes, giving a broad summary of the topics of the evaluation and the general situation of the evaluee

### Uses

In combination with the [residence record](residencerecord.html), this file is used to determine automatically who is due for evaluation before each [CoMe](../social/come.html).

This file can also be consulted by volunteers and members who were unable to attend an evaluation, to get a gist of what was said.

Occasionally, this file is analyzed to gather statistics about our position system or attendance of evaluations.

