# :family: Slack

We use a Slack workspace to organize ourselves, at [https://yunity.slack.com/](https://yunity.slack.com/)

## Creating a personal account

You can get an account by following [this link](https://slackin.yunity.org/).

## Channels

The Slack workspace we use is shared with other projects (notably the development of the foodsharing platform).
Our main channel is `#kanthaus`.
The other channels we use are mostly all prefixed by `#kanthaus-`.

Some of those channels are private, because they contain private or sensitive information. Those are reserved to volunteers and members.

## Migrating out of Slack

We are considering migrating to another platform.
