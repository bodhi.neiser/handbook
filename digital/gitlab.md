# :clipboard: GitLab

We use GitLab to store mutable data. For immutable files we have a [Nextcloud](nextcloud.md)

### What is GitLab?
GitLab is basically a social network built on top of the [git technology](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics) for version control. It is normally used for code, because it makes it easy to collaborate on common projects without breaking anything.

### Why did you decide to use this?
- We want to help people learn things and appreciate technology!
- Data that was put in git is extremely secure, because it's stored on multiple people's computers.
- If multiple people work on the same file it's possible to tell who changed which exact character.
- It's easy to put content from GitLab onto our website.
- Everybody becomes a webmaster. :)
- The people who decided on this were already used to it... ;)

### What is stored where?
So far we have the following repositories:
- [kanthaus.online](https://gitlab.com/kanthaus/kanthaus.online), where we store our website, [https://kanthaus.online/](https://kanthaus.online/).
- [kanthaus-private](https://gitlab.com/kanthaus/kanthaus-private), where we store our [residence record](residencerecord.md), internal meeting minutes, financial plans and other stuff that contains personal information. Everything of relevance that consists of text and has pieces of confidential data should be put here.
- [kanthaus-public](https://gitlab.com/kanthaus/kanthaus-public), which is publicly accessible as the name suggests. In this repository we do our task management via issues (which you can find [here](https://gitlab.com/kanthaus/kanthaus-public/issues)). This is especially important for our [roadmap plannings](../social/roadmap.md). Other than that we have drafts, policies, signs, data on our water, electricity and gas usage and much more in here.
- [kanthaus-ansible](https://gitlab.com/kanthaus/kanthaus-ansible) is for keeping our server tidy. Nothing you need to worry about, except you're actually interested. In that case feel free to ask Matthias about it!
- [handbook](https://gitlab.com/kanthaus/handbook) is where this handbook is stored.
- [expfloorer](https://gitlab.com/kanthaus/expfloorer) hosts a floorplan of Kanthaus.

