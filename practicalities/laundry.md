# :dress: Laundry

Everything concerning laundry - be it dirty or clean.

### Communal clothing

Since everything in Kanthaus is communal by default, this is the main section.

#### Dirty laundry sorting

Dirty laundry can be put into the laundry containers in the main bathroom (K20-2-1b). When those are full they go to the fitting sections in K20-0-1b.

The categories are as follows:
- 40°C: Standard laundry (e.g. most clothes)
- 60°C: Hygiene laundry (e.g. underwear, socks, tissues, bedsheets, towels)
- 90°C: Kill-everything laundry (e.g. kitchen towels, butt towels)

#### Wet laundry

Spaces to dry laundry are:
- the outside clothes lines
- the bathroom in K22-2-2
- if need be more mobile drying racks can be put up in ventilated rooms

#### Dry laundry

Clothes baskets can be found in the [washing room](/rooms/washingroom.md). Often they are also left right beside the drying rack and can be used to take down the laundry when dry. Dry communal laundry goes into the [communal closet](/rooms/communalcloset.md) and can just be put on the floor.

### Private clothing

If you stay only for a short time or have clothes which you don't want to share, you should not put them in the laundry baskets - chances are high that you won't find you stuff again in a reasonable amount of time. Simply run a machine yourself; it should be adequatly labeled to make it doable. Please make sure that you have enough things to wash, so that the machine is full, or add some Kanthaus pieces.

When drying your private clothing make sure you somehow label them as yours. Otherwise they might end up in the [communal closet](/rooms/communalcloset.md) after all.
