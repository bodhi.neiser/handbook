# :train: Travel
_Last updated: 2022-10-16_

As much as you love Kanthaus, you should get out now and then!

### Public Transport
:information_source: Beginning 01 Jan 2023, the 49 € ticket should be in action! This will allow you to take all buses, trams and non-ICE trains (e.g. S-Bahn, RE) across Germany for 49 € per month! :information_source:

:information_source: If you fancy a bit of direct action, you can ride illegally-but-supported with the [9eurofonds.de](https://9eurofonds.de) campaign on buses, trams and S-Bahn for a 9 € / month contribution to the fines-fund :information_source:

Wurzen, Leipzig, Halle, Merseburg and Oschatz all lie within the [Mitteldeutscher Verkehrsverbund](https://www.mdv.de/) (MDV). There is a nice map of the whole zone and subzones [here](https://www.mdv.de/site/uploads/tarifzonenplan.pdf). A lot of us really like the [Öffi](https://oeffi.schildbach.de/) transport app for journey plannning which is free and open-source, available on F-Droid and Google Play.

#### To Leipzig
The train is most convenient, with the station only 200 m from Kanthaus with 3 connections per hour. The S3 leaves at xx:18 and xx:48 and ~~normally takes ~45 minutes~~ currently takes ~25 minutes. The RE 50 leaves at xx:31 and takes ~20 minutes. The following tickets are valid for both trains.

:warning: You may get fined if your ticket is not "entwertet". If your ticket doesn't have a date-time, you need to punch it into one of the stamp-machines :warning:

The **MDV Einzelfahrkarte** is a one-way ticket that can be bought at any time, online or at the machines, for **7,10 €**. 
- This fare can be reduced using the [BahnCard 25](https://www.bahn.com/en/offers/bahncard/bahncard_25-conditions) to a **Normalpreis mit BahnCard-Rabatt Einfach** for **5,85 €**. The BahnCard 25 discount card costs 56.90 € for one year.
- This fare can be reduced using the [ABO Flex](https://abo.l.de/ABO_beantragen) to an **MDV Einzelfahrkarte ABO Flex** for **4,80 €**. The ABO Flex discount card costs 6,90 € / month, with a minimum period of 6 months.
- The discounts can't be combined.

Alternatively, the **MDV Hoppertickets** are **only available online** via the [bahn.de](https://bahn.de) website or app, or the [MOOVME](https://www.moovme.de/). They are only available between **09:00 - 04:00** on weekdays, no restrictions on the weekend.
- **MDV Hopperticket Einzelfahrt: 6,70 €** (one-way)
- **MDV Hopperticket Hin- und Rückfahrt: 11,00 €** (return: the return journey must be made before 04:00 the next day)
- These tickets are unaffected by BahnCards or ABO Flex's

The fares are cheaper from Bennewitz Bhf which is ~3.3 Km ([~19 minutes bike ride](https://www.qwant.com/maps/routes/?mode=cycling&destination=osm%3Anode%3A2299420210%40Bennewitz&pt=true&origin=osm%3Away%3A982897065%40Kanthaus&selected=0#map=14.44/51.3621209/12.7247490)) from Kanthaus, with the MDV Einzelfahrkarte costing 5,40 € and the MDV Einzelfahrkarte ABO Flex: 3,20 €. The Hopper ticket isn't any cheaper :shrug:

There also seems to be a bus which goes from Wurzen to Leipzig Sommerfeld, the 691, but the times going [there](https://www.mdv.de/site/uploads/fahrplaene/rl/220829_rl_691_h.pdf) and [back](https://www.mdv.de/site/uploads/fahrplaene/rl/220829_rl_691_r.pdf) are quite irregular, Sommerfeld is far from the center and the price is unknown—not recommendable.
