# :wind_chime: Airing

Rooms need to be aired for some reasons:
* Decrease in air quality (when people reside in there)
* Getting smell/dust out (Working, painting, cooking)
* Bringing humidity to a lower level to avoid condensation/mold (mainly when the average outside temperatures are below the average inside temperatures)

Airing is most efficient when–
- windows are fully opened and not just partially.
- windows on both sides of the building are opened simultaneously, so that wind can go through.
- done more often but less long, especially when it's cold. This way the air in the room can be exchanged without losing too much heat.

More detailed information on this can be found in [this guide on how to handle doors and windows](doorwindowhandling.md).
