# :izakaya_lantern: Dining Room (or K20-1-3)

The dining room is a room with a lot of table and sitting space, to accommodate many people relaxing and/or eating.

### Facilities
- An extensible table with either 4-6 or 8-10 places.
- A large but dynamically changing number of chairs.
- An big couch.
- Some cupboards and shelves that hold games, books, Mika things and much more...

### Room availability
This is a public room, so it is always accessible to people. Sleeping in here is probably a bad idea and might irritate some people.

### Specials
Since this is one of the most public rooms, please make sure to keep the tables free of personal belongings if they are not in immediate use!
